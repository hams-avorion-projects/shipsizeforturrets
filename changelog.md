# Changelog
### 1.0.1
* Fixed issues with Avorion 1.3
* Fixed changelog file

### 1.0
* Updated mod for Avorion 2.x support
* Added bonuses for point defense and automatic turret slots
* Updated default mod config to match new processing power limit (only affects new galaxies)

### 0.1.0
* Initial release

