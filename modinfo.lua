
meta =
{
    id = "2061614780",
    name = "ShipsizeForTurrets",
    title = "Shipsize ForTurrets",
    description = "Increase maximum amount of turrets for player ships, the more processing power the ship has.",
    authors = {"Hammelpilaw"},
    version = "1.0.1",
    dependencies = {
        {id = "Avorion", max = "2.*"},
		{id = "1720259598", min = "0.3.1"}, -- ConfigLib
		--{id = "1722412006", min = "0.0.1"}, -- ExtUtility
    },
    serverSideOnly = true,
    clientSideOnly = false,
    contact = "info@scrap-yard.org",
}
