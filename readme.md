# ShipsizeForTurrets

Increase the maximum amount of arbitrary, defense and auto turrets for player ships (and stations), the more processing power the ship has.

With default configuration the max bonus of 50 arbitrary, 20 defense and 20 auto turrets will be reached at 190k processing power. Max bonuses and how much processing power you need for can be changed in configuration file inside of the galaxy folder:

    /moddata/ConfigLib/ShipsizeForTurrets-2061614780.lua

Note: this file will be created on first server/galaxy startup having this mod enabled.