package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local ConfigLib = include("ConfigLib")
local SFTConfigLib = ConfigLib("2061614780")

-- Don't remove or alter the following comment, it tells the game the namespace this script lives in. If you remove it, the script will break.
-- namespace Sizeforturrets
Sizeforturrets = {}

Sizeforturrets.numTurretBonus = 0 -- Deprecated

Sizeforturrets.bonusMultiplier = 1

Sizeforturrets.numMaxTurrets = 0
Sizeforturrets.numMaxDefTurrets = 0
Sizeforturrets.numMaxAutoTurrets = 0
Sizeforturrets.prossessingCap = 0

-- this function gets called on creation of the entity the script is attached to, on client and server
function Sizeforturrets.initialize()
	if onServer() then
		Entity():registerCallback("onBlockPlanChanged", "onBlockPlanChanged")
		
		Sizeforturrets.numMaxTurrets = SFTConfigLib.get("numMaxTurrets")
		Sizeforturrets.numMaxDefTurrets = SFTConfigLib.get("numMaxDefTurrets")
		Sizeforturrets.numMaxAutoTurrets = SFTConfigLib.get("numMaxAutoTurrets")
		Sizeforturrets.prossessingCap = SFTConfigLib.get("prossessingCap")

        -- execute the callback for initialization, to be sure
        Sizeforturrets.onBlockPlanChanged(Entity().id, true)
	end
end



function Sizeforturrets.onBlockPlanChanged(entityId, allBlocks)
	local processingFactor = Plan():getStats().processingPower / 1000000 -- We calculate in mio
	local prossessingCap = SFTConfigLib.get("prossessingCap") -- In mio
	local numMaxTurrets = SFTConfigLib.get("numMaxTurrets")
	local numMaxDefTurrets = SFTConfigLib.get("numMaxDefTurrets")
	local numMaxAutoTurrets = SFTConfigLib.get("numMaxAutoTurrets")
	
	Sizeforturrets.bonusMultiplier = (10*(processingFactor/prossessingCap)+40*(processingFactor/prossessingCap)^0.4)
	
	Sizeforturrets.applyStatsBonus()
end


-- Aplly the stats bonus
-- Use mod ID as bonus ID
function Sizeforturrets.applyStatsBonus()
	local entity = Entity()
	
	local numTurrets = math.floor(math.min(Sizeforturrets.numMaxTurrets, Sizeforturrets.bonusMultiplier * Sizeforturrets.numMaxTurrets / 50)) -- Devide by 50 cause we sadly calculated at a 50 turrets bonus base
	local numDefTurrets = math.floor(math.min(Sizeforturrets.numMaxDefTurrets, Sizeforturrets.bonusMultiplier * Sizeforturrets.numMaxDefTurrets / 50)) -- Devide by 50 cause we sadly calculated at a 50 turrets bonus base
	local numAutoTurrets = math.floor(math.min(Sizeforturrets.numMaxAutoTurrets, Sizeforturrets.bonusMultiplier * Sizeforturrets.numMaxAutoTurrets / 50)) -- Devide by 50 cause we sadly calculated at a 50 turrets bonus base
	
	SFTConfigLib.log(5, "Add stat bonus:", "arbitrary" .. numTurrets .. ";", "def" .. numDefTurrets .. ";", "auto" .. numAutoTurrets .. ";")
	
	entity:removeBonus(20616147800) -- Use mod id + suffix for best compatibility
	entity:removeBonus(20616147801)
	entity:removeBonus(20616147802)
	entity:addKeyedMultiplyableBias(StatsBonuses.ArbitraryTurrets, 20616147800, numTurrets)
	if GameVersion() > Version(1, 999, 999) then -- 2.*
		entity:addKeyedMultiplyableBias(StatsBonuses.PointDefenseTurrets, 20616147801, numDefTurrets)
		entity:addKeyedMultiplyableBias(StatsBonuses.AutomaticTurrets, 20616147802, numAutoTurrets)
	end
end

