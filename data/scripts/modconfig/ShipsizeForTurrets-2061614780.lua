return {
	{ name ="numMaxTurrets",					value = 50,		description = "Maximum amount of turrets this mod will add to a single ship.'" },
	{ name ="numMaxDefTurrets",					value = 20,		description = "Maximum amount of turrets this mod will add to a single ship.'" },
	{ name ="numMaxAutoTurrets",				value = 20,		description = "Maximum amount of turrets this mod will add to a single ship.'" },
	{ name ="prossessingCap",					value = 0.190,	description = "Required processing power (in Mio.) to reach the max of 'numMaxTurrets.'" },
}